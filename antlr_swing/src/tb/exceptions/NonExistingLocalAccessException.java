/**
 * 
 */
package tb.exceptions;

/**
 * @author izwir
 *
 */
public class NonExistingLocalAccessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 802888965559396964L;

	/**
	 * 
	 */
	public NonExistingLocalAccessException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public NonExistingLocalAccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public NonExistingLocalAccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NonExistingLocalAccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NonExistingLocalAccessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
