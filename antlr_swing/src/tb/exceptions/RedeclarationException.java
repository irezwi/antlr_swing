/**
 * 
 */
package tb.exceptions;

/**
 * @author izwir
 *
 */
public class RedeclarationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2111633810771729331L;

	/**
	 * 
	 */
	public RedeclarationException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public RedeclarationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public RedeclarationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RedeclarationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedeclarationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
