/**
 * 
 */
package tb.exceptions;

/**
 * @author izwir
 *
 */
public class ZeroDivisionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3985482017536335066L;

	/**
	 * 
	 */
	public ZeroDivisionException() {
		super("Cannot divide by zero!");
	}

	/**
	 * @param message
	 */
	public ZeroDivisionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ZeroDivisionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ZeroDivisionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ZeroDivisionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
