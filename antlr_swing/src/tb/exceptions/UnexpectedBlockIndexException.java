/**
 * 
 */
package tb.exceptions;

/**
 * @author izwir
 *
 */
public class UnexpectedBlockIndexException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7842234317846160251L;

	/**
	 * 
	 */
	public UnexpectedBlockIndexException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UnexpectedBlockIndexException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UnexpectedBlockIndexException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnexpectedBlockIndexException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnexpectedBlockIndexException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
