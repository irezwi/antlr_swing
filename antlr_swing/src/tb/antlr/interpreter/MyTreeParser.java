package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;
import tb.exceptions.*;

import java.util.HashMap;

public class MyTreeParser extends TreeParser {
	
	GlobalSymbols globals = new GlobalSymbols();
	LocalSymbols locals = new LocalSymbols();
	
	Integer currentBlockNumber = 1;
	
	Boolean DEBUG = false;	

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
    	String printedText = text != null ? text : "";
		System.out.println(printedText);
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer div(Integer a, Integer b) throws RuntimeException {
		if (b == 0)
			throw new ZeroDivisionException();
		return a / b;		
	}
	
	protected Integer pow(Integer number, Integer power) {
		return (int) Math.pow(number, power);
	}
	
	private String getNameWithBlock(String name) {
		StringBuilder sb = new StringBuilder();
		return sb.append(name).append("_").append(this.currentBlockNumber).toString();
	}
	
	private String getNameWithBlock(String name, Integer customScopeIndex) {
		StringBuilder sb = new StringBuilder();
		return sb.append(name).append("_").append(customScopeIndex).toString();
	}
	
	/* DISPATCH TO GLOBAL OR LOCAL */
	protected void declareVar(String varName) {
		if (this.currentBlockNumber == 1)
			declareGlobal(varName);
		else if (this.currentBlockNumber > 1)
			declareLocal(varName);
		else
			throw new RuntimeException("Unexpected block number!");
	}
	
	protected void assignToVar(String varName, Integer value) {	
		if (this.currentBlockNumber == 1)
			assignToGlobal(varName, value);
		else if (this.currentBlockNumber > 1)
			assignToLocal(varName, value);
		else
			throw new UnexpectedBlockIndexException("Unexpected block number!");
	}
	
	protected Integer getVar(String varName) {
		if (this.currentBlockNumber == 1)
			return getGlobal(varName);
		else if (this.currentBlockNumber > 1) {
			int block = this.currentBlockNumber;
			do {
				try {
					return this.getLocal(varName, block);
				} catch (NonExistingLocalAccessException nelae) {
					if (this.DEBUG) System.out.println("Nie znaleziono: " + varName + " w bloku " + block);
					continue;
				}
			} while (block-- > 1);
			return getGlobal(varName); // Access to global variable in local scope
		}
		else
			throw new UnexpectedBlockIndexException("Unexpected block number!");
	}
	
	/* GLOBALS */
	protected void declareGlobal(String globalVariableName) {
		if (this.DEBUG) System.out.println("Deklaruje globalna: " + globalVariableName);
		this.globals.newSymbol(globalVariableName);
	}
	
	protected void assignToGlobal(String globalVariableName, Integer value) {
		if (this.DEBUG) System.out.println("Przypisuje globalna: " + globalVariableName);
		this.globals.setSymbol(globalVariableName, value);
	}
	
	protected Integer getGlobal(String globalVariableName) {
		if (this.DEBUG) System.out.println("Podaje globalna");
		return this.globals.getSymbol(globalVariableName);
	}
	
	/* LOCALS */
	protected void declareLocal(String variableName) throws RedeclarationException {
		String varNameWithBlock = this.getNameWithBlock(variableName);
		if (!this.locals.hasSymbol(varNameWithBlock)) {
			if (this.DEBUG) System.out.println("Deklaruje lokalna: " + varNameWithBlock);
			this.locals.newSymbol(varNameWithBlock);
		}
		else {
			String msg = String.format("Variable %s is already declared in this scope!", variableName);
			throw new RedeclarationException(msg);
		}
	}
	
	protected Integer getLocal(String variableName) throws RuntimeException {	
		if (this.DEBUG) System.out.println(String.format("Get %s called", variableName));
		
		HashMap<String, Integer> hm = this.locals.hasSymbolDepth(variableName);
		if (hm != null)
			return hm.get(variableName);
		else
			throw new NonExistingLocalAccessException("Cannot get non-existing variable");
	}
	
	protected Integer getLocal(String variableName, Integer blockIndex) throws RuntimeException {		
		String varNameWithBlock = this.getNameWithBlock(variableName, blockIndex);
		return this.getLocal(varNameWithBlock);
	}
	
	protected void assignToLocal(String variableName, Integer value) throws RuntimeException {	
		String varNameWithBlock = this.getNameWithBlock(variableName);
		
		if (this.locals.hasSymbol(varNameWithBlock)) {
			if (this.DEBUG) System.out.println("Przypisuje lokalna: " + varNameWithBlock + " " +
											   "w bloku: " + this.currentBlockNumber);
			this.locals.setSymbol(varNameWithBlock, value);
		}
		else
			throw new NonExistingLocalAccessException("Cannot assign to non-existing variable");
	}
	
	protected Integer enterScope() {
		return this.locals.enterScope();
	}
	
	protected Integer leaveScope() {
		return this.locals.leaveScope();
	}
}
