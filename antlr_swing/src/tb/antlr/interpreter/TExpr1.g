tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    
        : (e=expr 
        | LCB {currentBlockNumber = enterScope();} 
        | RCB {currentBlockNumber = leaveScope();}
        )*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
        | ^(PRINT e1=expr)         {drukuj($e1.out.toString());}
        | ^(VAR name=ID)           {declareVar($name.text);}
        | ^(PODST name=ID e=expr)  {assignToVar($name.text, $e.out);}
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
